﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Hex_Viewer {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e) {
            DialogResult drOpenResult = ofdOpenFile.ShowDialog();
            if(drOpenResult == DialogResult.OK) {
                string strPath = ofdOpenFile.FileName;
                FileStream fsFile = File.Open(strPath, FileMode.Open, FileAccess.Read);
                nudStartingPara.Value = 0;
                nudStartingPara.Maximum = iMaxStartPara(fsFile.Length);
                vDisplayData(fsFile, 0);
                fsFile.Close();
                // Safe to enable the numeric up-down.
                nudStartingPara.Enabled = true;                
            }
        }

        // Function to calculate the maximum value for the starting paragraph number.
        private int iMaxStartPara(long lFileLen) {
            double dblNumParas = Math.Ceiling(Convert.ToDouble(lFileLen) / 16.0);
            int iHighStartPara = Convert.ToInt32(dblNumParas) - 8;
            // Make sure to return a nonnegative number
            return Math.Max(0, iHighStartPara);

        }

        // Function to display 8 paragraphs from the file starting at the given paragraph.
        private void vDisplayData(FileStream fsFile, decimal decStartPara) {
            dgvData.Rows.Clear();
            // Move file position pointer to start of starting paragraph.
            long lStartPara = Convert.ToInt64(decStartPara);
            long lStartByte = lStartPara * 16;
            fsFile.Seek(lStartByte, SeekOrigin.Begin);
            // Loop through, read and display the next 8paragraphs.
            byte[] byBuffer = new byte[16];
            for (int iCount = 0; iCount < 8; iCount++) {
                // Read the next paragraph, display it.
                int iBytesRead = fsFile.Read(byBuffer, 0, 16);
                vDisplayOnePara(byBuffer, iBytesRead, iCount + lStartPara);
            }

        }
         // Function to display one paragraph of data.
         private void vDisplayOnePara(byte[] byBuffer, int iNumBytes, long lParaNum) {
            // Build up the hex and text strings from the data byte by byte.
            string strHex = "";
            string strText = "";
            // Loop through the bytes in the data array.
            for (int iByte = 0; iByte < iNumBytes; iByte++) {
                // Get the current byte
                byte byCurrByte = byBuffer[iByte];
                // Convert byte to hex and to text and append to appropriate strings
                strHex += byCurrByte.ToString("X2") + " ";
                strText += strByteToChar(byCurrByte);
                if (iByte == 7) {
                    strHex += " ";
                    strText += " ";
                }
            }
            // Create an array to hold the data to display in the next row of the DGV.
            object[] objData = new object[3];
            objData[0] = lParaNum;
            objData[1] = strHex;
            objData[2] = strText;
            // Add a new row to the DGV containing the data in the array.
            dgvData.Rows.Add(objData);

        }

        // Convert a byte to an ASCII Character if it is one, otherwise convert it to a dot.
        private string strByteToChar(byte byByte) {
            // Printable ASCII Characters are those with encodings between 32 and 126.
            if (32 <= byByte && byByte <= 126) {
                return Convert.ToChar(byByte).ToString();
            } else {
                return ".";
            }
        }

        private void nudStartingPara_ValueChanged(object sender, EventArgs e) {
            // Similar to when we click the Open option in the file menu, but we don't need to choose the file again.
            string strPath = ofdOpenFile.FileName;
            FileStream fsFile = File.Open(strPath, FileMode.Open, FileAccess.Read);
            vDisplayData(fsFile, nudStartingPara.Value);
            fsFile.Close();
        }
    }
}

